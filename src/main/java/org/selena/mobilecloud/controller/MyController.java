package org.selena.mobilecloud.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
public class MyController {

    @RequestMapping(value="/", method= RequestMethod.GET)
    public String start( Model model, final Principal p) {
        String userName= p.getName();
        model.addAttribute("username", userName);
        return "start";
    }

    @PreAuthorize( "hasRole('admin')")
    @RequestMapping(value="/admin", method= RequestMethod.GET)
    public String admin( Model model, final Principal p) {
        String userName= p.getName();
        model.addAttribute("username", userName);
        return "admin";
    }

    @PreAuthorize( "#siteName == authentication.name or hasRole('admin')" )
    //@PreAuthorize( "#siteName == #p.name")
    @RequestMapping(value="/{name}/view/**", method= RequestMethod.GET)
    public String viewUsername( Model model, @PathVariable("name") final String siteName, final Principal p) {
        String userName= p.getName();
        model.addAttribute("username", userName);
        model.addAttribute("siteName", siteName);
        return "user-custom";
    }

}
